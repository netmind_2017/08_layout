package com.example.gelso.layout;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;


public class Main5Activity extends AppCompatActivity {

    Button btn9;
    Button button2;
    TextView text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        btn9 = (Button) findViewById(R.id.btn9);
        text1 = (TextView) findViewById(R.id.textView2);

        //BTN
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text1.setBackgroundColor(Color.RED);

            }
        });

    }
}
