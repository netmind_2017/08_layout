package com.example.gelso.layout;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    Button btn3,btn4,btn5,btn6,btn7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btn3 = (Button) findViewById(R.id.btn3);

        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);


        //BTN3
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn3.setBackgroundColor(Color.RED);

            }
        });




        //BTN5
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn5.setBackgroundColor(Color.BLUE);

            }
        });


        //BTN6
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn6.setBackgroundColor(Color.YELLOW);

            }
        });

        //BTN7
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), Main3Activity.class);
                i.putExtra("param1","Hello" );
                i.putExtra("param2","Word");
                startActivity(i);
            }
        });

    }
}
